# Vue Gutenberg Render Component

Этот компенент парсит и рендерит Wordpress Gutenberg блоки (например пришедшие с сервера):

```
...
{
  "blocks": [
    {
      "blockName": "danifo/anchor",
      "attrs": {
        "id": "glavnaia",
        "label": "Главная"
      },
      "innerBlocks": [],
      "innerHTML": ""
    },
    {
      "blockName": null,
      "attrs": [],
      "innerBlocks": [],
      "innerHTML": "\n\n",
      "innerContent": ["\n\n"]
    }
  ]
}
...
```

### Установка

`npm install https://gitlab.com/drakeps/vue-gutenberg-render.git#2.0.0`

### Использование

##### Для начала создадим компонент-рендер `Button.vue`:

```
<template>
  <div>
    <b-button :type="block.attrs.type">{{ block.attrs.text }}</b-button>
  </div>
</template>

<script>
export default {
  blockName: "danifo/container", // Обязательный параметр. Это название блока в wordpress gutenberg
  props: ["block"] // через props получаем данные блока
}
</script>
```

##### Подключение:

```
import Vue from "vue";
import GutenbergRender from "vue-gutenberg-render";
import ButtonRender from "./ButtonRender";

Vue.use(GutenbergRender, { ButtonRender });
// Vue.use(GutenbergRender, { ContainerRender, ButtonRender, TitleRender, ... });
```

##### Использование в шаблоне:

```
<gutenberg-render :blocks="postBlocks" class="some-cool-content"/>
```

## Важно !!

#### Чтобы `render` работал в `nuxt.js`, нужно в файле `nuxt.config.js` добавить:

```
  ...

  build: {
        extend(config, ctx) {
            // Include the compiler version of Vue so that wp-content works
            config.resolve.alias["vue$"] = "vue/dist/vue.esm.js"
        }
    }

  ...
```
