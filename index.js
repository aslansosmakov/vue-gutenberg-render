module.exports = {
  install(Vue, blockRenderComponents) {
    // Make components object => blockRenderComponentName : component
    // it need to add 'gutenberg-render' prefix to components names
    let components = {}
    for (let component in blockRenderComponents) {
      components['gutenberg-render-' + component] = blockRenderComponents[component]
    }

    let GutenbergRender = {
      name: "GutenbergRender",
      
      props: {
        blocks: {
          type: Array,
          required: true
        }
      },
      components,

      methods: {
        getBlockRenderComponentName(blockName) {
          for (let [componentName, component] of Object.entries(components)) {
            if (component.blockName === blockName) {
              return componentName
            }
          }
          return null
        },

        renderedBlocks(h) {
          let renderBlock = blocks => {      
            return blocks
              .filter(block => block.blockName !== null)
              .map(block => {
                // if we have render component for current block, render it
                let renderComponentName = this.getBlockRenderComponentName(block.blockName)
                if (renderComponentName) {
                  return h(renderComponentName, {
                    props: { block }
                  })
                }

                if (block.innerBlocks.length) {
                  let innerBlocksRendered = renderBlock(block.innerBlocks)
                  let template = block.innerContent
                    .map(item => item || innerBlocksRendered.shift())
                    .join("")
                  
                  return h({template})
                }

                // if it simple block html and has content
                if (block.innerContent.length) {
                  return h({template: block.innerContent.join("")})
                }

                return ''
              })
          }

          return renderBlock(this.blocks)
        }
      },

      render(h) {
        return h('div', this.renderedBlocks(h))
      }
    }

    Vue.component("gutenberg-render", GutenbergRender)
  }
}
